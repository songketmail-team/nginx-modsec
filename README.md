# Nginx + ModSecurity Docker Image

Ada pun projek ini bermula pada asalnya adalah untuk memudahkan kerja-kerja menambah ModSecurity ke dalam Nginx.

Dengan adanya docker image seperti ini, diharap kerja menyelenggara Nginx + ModSecurity dapat dipermudah dan dipercepatkan tanpa melibatkan kerja compile Nginx + ModSecurity di server yang akan mengambil masa yang lama dan ada kalanya compile failed kerana tak cukup library.

Dengan Docker image ini juga, Server boleh menjadi minimal tanpa pemasangan developer tool dan libraries untuk compile.

Hanya perlu pull docker image ini dan gunakan.

## Cara Penggunaan

Untuk menggunakan image docker ini, perlu login dulu ke `gitlab registry` seperti berikut:
```bash
docker login registry.gitlab.com
```
Masukkan username dan password gitlab, kemudian bolehlah pull image terkini.

```bash
docker pull registry.gitlab.com/apogeek/nginx-modsec
```

Bagi memudahkan penggunaannya, boleh juga tag image ini dalam local image agar namanya menjadi pendek setelah `pull` dari registry dengan arahan seperti berikut:
```bash
docker tag registry.gitlab.com/apogeek/nginx-modsec:latest nginx-modsec:latest
```

Setelah itu, image ini boleh dilarikan dengan arahan berikut:
```bash
docker run -p 80:80 nginx-modsec
```

atau panggil dari `docker-compose.yml`:
```yaml
services:
    secnginx:
        image: nginx-modsec:latest
        ports:
            - 80:80
            - 443:443
```

## Perancangan

Atas faktor keselamatan, saya menjangkakan adalah menjadi satu kewajipan untuk memasang nginx dengan ModSecurity pada setiap server production.

Perancangan saya pada ketika ini ialah untuk menambah setting lain yang dapat mempermudah dan mempercepatkan kerja setup **hardened web server** di masa hadapan.
