#!/bin/bash

echo "== Nginx 1.19.0 + ModSecurity builder ~ by APOGEEK =="

tar zcvf build/nginx-conf.tar.gz -C build nginx
docker build -t nginx-modsec:1.19.0 ./build
